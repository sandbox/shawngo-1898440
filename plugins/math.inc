<?php

/**
 * @file
 * Add mathematical operations.
 */

$plugin = array(
  'form' => 'feeds_tamper_extended_math_form',
  'callback' => 'feeds_tamper_extended_math_callback',
  'validate' => 'feeds_tamper_extended_math_validate',
  'name' => 'Math Expression',
  'multi' => 'loop',
  'category' => 'Number',
);

function feeds_tamper_extended_math_form($importer, $element_key, $settings) {
  $form = array();
  $form['math']['#markup'] = t('Perform mathematical calculations in the form: <strong>Feed Value</strong> <em>Operator</em> <strong>Value</strong>. The result is returned to feeds.');
  $form['number'] = array(
    '#type' => 'select',
    '#title' => t('Number type'),
    '#options' => array('integer', 'double', 'float'),
    '#default_value' => isset($settings['number']) ? $settings['number'] : '',
  );
  $form['operator'] = array(
    '#type' => 'select',
    '#title' => t('Operator'),
    '#options' => array('add' => '+', 'subtract' => '-', 'multiply' => '*', 'divide' => '÷'),
    '#default_value' => isset($settings['operator']) ? $settings['operator'] : '',
  );
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#default_value' => isset($settings['value']) ? $settings['value'] : '',
    '#description' => t('Enter the value to balance the equation as an integer or decimal.'),
    '#size' => 5,
  );
  return $form;
}

function feeds_tamper_extended_math_validate(&$settings) {
  $settings['value'] = trim($settings['value']);
  if ($settings['value'] === '0' && $settings['operator'] == 'divide') {
    form_set_error('settings][value', t('Cannot divide by zero.'));
  }
}

function feeds_tamper_extended_math_callback($result, $item_key, $element_key, &$field, $settings) {
  $value = $field;
  switch ($settings['number']) {
    case 'integer':
      $value = intval($value);
      break;
    case 'double':
      $value = doubleval($value);
      break;
    case 'float':
      $value = floatval($value);
      break;
  }
  switch ($settings['operator']) {
    case 'add':
      $field = $value + $settings['value'];
      break;
    case 'subtract':
      $field = $value - $settings['value'];
      break;
    case 'multiply':
      $field = $value * $settings['value'];
      break;
    case 'divide':
      $field = $value / $settings['value'];
      break;
  }
}
