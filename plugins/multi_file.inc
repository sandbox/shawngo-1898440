<?php

/**
 * @file
 * Define additional custom tamper plugins.
 */

$plugin = array(
  'form' => 'feeds_tamper_extended_multi_file_form',
  'callback' => 'feeds_tamper_extended_multi_file_form_callback',
  'name' => 'Multiple files',
);

function feeds_tamper_extended_multi_file_form($importer, $element_key, $settings) {
  $form = array();
  $form['file_protocol'] = array(
    '#type' => 'radios',
    '#title' => t('Download type'),
    '#options' => array('none' => t('None'), 'public' => t('public://'), 'private' => t('private://')),
    '#default_value' => isset($settings['file_protocol']) ? $settings['file_protocol'] : 'none',
  );
  $form['explode_on'] = array(
    '#type' => 'textfield',
    '#title' => t('Explode on'),
    '#description' => t('Enter the field separator to explode on. If nothing is entered, no exploding will happen.'),
    '#default_value' => isset($settings['explode_on']) ? $settings['explode_on'] : '',
    '#size' => 4,
    '#required' => FALSE,
  );
  $form['download_directory'] = array(
    '#type' => 'textfield',
    '#title' => t('Download directory'),
    '#size' => 40,
    '#description' => t('Enter the directory within sites/default/files where the source file exists. If empty, defaults to sites/default/files or blank.'),
    '#default_value' => isset($settings['download_directory']) ? $settings['download_directory'] : '',
  );
  return $form;
}

function feeds_tamper_extended_multi_file_form_callback($result, $item_key, $element_key, &$field, $settings) {
  $tmp_field = $field;
  if ($settings['explode_on']) {
    $values = explode($settings['explode_on'], $tmp_field);
  }
  else {
    $values = array($tmp_field);
  }
  if ($settings['file_protocol']) {
    $prefix = $settings['file_protocol'] . '://';
    if ($settings['download_directory']) {
      $prefix .= '/' . $settings['download_directory'] . '/';
    }
  }
  foreach ($values as &$value) {
    $value = $prefix . $value;
  }
  $field = implode('', $values);
}
